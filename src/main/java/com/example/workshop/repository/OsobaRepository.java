package com.example.workshop.repository;

import com.example.workshop.entity.Osoba;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OsobaRepository extends JpaRepository<Osoba, Integer> {
}
