package com.example.workshop.controller;

import com.example.workshop.repository.OsobaRepository;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class MainController {

    private OsobaRepository osobaRepository;

    @GetMapping("/")
    public String getIndex() {
        return "Hello";
    }

    @GetMapping("/osoba")
    public String getOsoba() {
        return osobaRepository.findAll().get(0).getName();
    }
}
